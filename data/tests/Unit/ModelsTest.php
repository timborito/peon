<?php

namespace Tests\Unit;

use function foo\func;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Peon\Group;
use Peon\Member;
use Peon\Task;
use Peon\TaskEvent;
use Peon\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

use Peon;


class ModelsTest extends TestCase
{
    //for rollback
    //use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */

    public function testBasicTest()
    {


        $user = User::find(1)->first();
        $groupAdmin = User::find(2)->first();

        $groupMember = User::where("email", "hengelore@mail.com")->get()->first();

        $client = DB::table("oauth_clients")->where("password_client", 1)->get()->first();
        if (is_null($client)) {
            return;
        }

        $response = $this->postJson("/oauth/token", [
            "grant_type" => "password",
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "scope" => "*",
            "username" => "admin@mail.com",
            "password" => "password"
        ]);
        $token = $response->json()["access_token"];

        $response = $this->postJson("/oauth/token", [
            "grant_type" => "password",
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "scope" => "*",
            "username" => "jeffrey@mail.com",
            "password" => "cat"
        ]);
        $tokenGroupAdmin = $response->json()["access_token"];

        $headers = [
            "Authorization" => "Bearer $token",
            "Accept" => "application/json",
        ];

        $headersGroupAdmin = [
            "Authorization" => "Bearer $tokenGroupAdmin",
            "Accept" => "application/json",
        ];


        // GET
        $response = $this->withHeaders($headers)->actingAs($user)->getJson("/api/group");
        $response->assertStatus(200);

        // GET FAIL
        $response = $this->withHeaders($headers)->actingAs($user)->getJson("/api/group/99999999");
        $response->assertStatus(404);
        // POST

        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group", [
            "title" => "Stupid Group",
        ]);
        $response->assertStatus(201);

        $groupID = $response->json()['id'];

        // POST MEMBER INTO GROUP
        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/member", [
            "user_id" => User::all()->random(1)->first()->id
        ]);
        $response->assertStatus(201);
        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/member", [
            "user_id" => $groupMember->id
        ]);

        $response->assertStatus(201);


        // POST MEMBER INTO GROUP FAIL
        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/member", [
        ]);
        $response->assertStatus(422);


        // GET AFTER POST
        $response = $this->withHeaders($headers)->actingAs($user)->getJson("/api/group/$groupID");
        $response->assertStatus(200)->assertJson([
            "id" => $groupID
        ]);
        //PUT
        $response = $this->putJson("/api/group/$groupID", [
            "title" => "very stupid group",
        ]);


        $response->assertStatus(200)->assertJsonFragment([
            "title" => "very stupid group"
        ]);


        $newUser = factory(User::class)->create();

        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/member", [
            "user_id" => $newUser->id,
            "credits" => 3,
        ]);
        $response->assertStatus(201);
        $memberID = $response->json()["id"];


        $this->assertTrue(Member::find($memberID)->first()->hasRole("member"));

        ///$this->assertFalse(Member::find($memberID)->first()->hasRole("group-admin"));

        $response = $this->withHeaders($headers)->actingAs($user)->putJson("/api/group/$groupID/member/$memberID", [
            "credits" => 50,
        ]);

        $response->assertStatus(200)->assertJsonFragment([
            "credits" => 50
        ]);

        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/taskEvent", [
            "title" => "Hurraaaaaa",
            "description" => "Aujaaaaaa!!",
            "color" => "#aabbcc",
            "credits" => "99",
            "order_index" => "2",
            "quit_on_expiration" => "true",
            "group_id" => $groupID,

            "expiration" => [
                "expires" => "12:00"
            ],

            "frequency" => [
                "day" => '6',
                "week" => '0',
                "month" => '0'
            ],
            "condition" => [
                "date" => "2018-09-14",
                "time" => "13:00",
            ],
        ]);

        $response->assertStatus(422);

        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/taskEvent", [
            "title" => "Hurraaaaaa",
            "description" => "Aujaaaaaa!!",
            "color" => "#aabbcc",
            "credits" => "99",
            "order_index" => "0",
            "quit_on_expiration" => true,
            "group_id" => $groupID,

            "expiration" => [
                "expires" => "12:00"
            ],
            "frequency" => [
                "day" => '6',
                "week" => '0',
                "month" => '0'
            ],
            "condition" => [
                "date" => "2018-09-14",
                "time" => "09:00",
            ],
        ]);

        $taskEventID = $response->json()["id"];
        $response->assertStatus(201);

        $response = $this->withHeaders($headers)->actingAs($user)->putJson("/api/group/$groupID/taskEvent/$taskEventID", [
            "title" => "Hurrks!",
            "expiration" => [
                "expires" => "09:00",
                "days" => 2,
            ],
            "frequency" => [
                "day" => '6',
                "week" => '0',
                "month" => '0'
            ],
            "condition" => [
                "date" => "2018-09-14",
                "time" => "05:00",
            ],
        ]);

        $response->assertStatus(200);

        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/verify");

        $response->assertStatus(200);

        $this->assertTrue(Task::where("task_event_id", $taskEventID)->get()->last()->expired_at == null);


        $response = $this->withHeaders($headers)->actingAs($user)->postJson("/api/group/$groupID/verify", [
            "on" => "2018-09-20"
        ]);
        $taskID = $response->json()[count($response->json()) - 1]["id"];
        $response->assertStatus(200);
        $this->assertTrue(Task::where("id", $taskID)->get()->last()->expired_at != null);


        $this->assertTrue(TaskEvent::find($taskEventID)->condition->time == "05:00:00");
        $response = $this->withHeaders($headers)->actingAs($user)->getJson("/api/group/$groupID/member/$memberID");
        $response->assertStatus(200);

        $response = $this->withHeaders($headersGroupAdmin)->actingAs($groupAdmin)->getJson("/api/group/$groupID");
        $response->assertStatus(200);

        $response = $this->withHeaders($headersGroupAdmin)->actingAs($groupMember)->getJson("/api/group/$groupID");
        $response->assertStatus(200);

        $response = $this->withHeaders($headersGroupAdmin)->actingAs($groupMember)->getJson("/api/group/2");
        //$response->assertStatus(401);

        $response = $this->withHeaders($headersGroupAdmin)->actingAs($groupAdmin)->post("/api/group/2/member", [
            "user_id" => $groupMember->id
        ]);
        $response->assertStatus(201);

        $response = $this->withHeaders($headersGroupAdmin)->actingAs($groupMember)->getJson("/api/group/2");
        $response->assertStatus(200);

        $response = $this->withHeaders($headersGroupAdmin)->actingAs($groupMember)->postJson("/api/group/$groupID/taskEvent/$taskEventID/swap", [
            "iteration_distance" => 3,
            "swap" => 6
        ]);
        $response->assertStatus(201);


        //DELETE
        /*
        $response = $this->withHeaders($headersGroupAdmin)->actingAs($groupAdmin)->deleteJson("/api/group/$groupID", [
            "id" => $groupID,
        ]);

        $response->assertStatus(200);
        // GET AFTER PUT
        $response = $this->getJson("/api/group/$groupID");
        $response->assertStatus(404);
        */
    }
}

@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">
    <style>
        .fc-content {
            padding: 10px;
        }
        .fc-day-grid-event{
            margin-bottom: 5px;
        }
        .fc-title{
            font-size: 14px;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        {!! $calendar->calendar() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {!! $calendar->script() !!}
@endsection
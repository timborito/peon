<?php

use Illuminate\Database\Seeder;
use Peon\Permission;
use Peon\Role;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Role::create([
            "slug" => "member",
            "display" => "Member"
        ]);


        $group = Role::create([
            "slug" => "group-admin",
            "display" => "Group Admin"
        ]);


        $admin = Role::create([
            "slug" => "admin",
            "display" => "Admin"
        ]);

    }
}

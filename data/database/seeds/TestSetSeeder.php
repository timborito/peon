<?php


use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Peon\Condition;
use Peon\Frequency;
use Peon\Group;
use Peon\Expiration;
use Peon\MemberRole;
use Peon\TaskOrder;
use Peon\TaskTag;
use Peon\Role;
use Peon\TaskEvent;
use Peon\User;
use Peon\Member;

class TestSetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        // GROUP

        $rumpel = User::create([
            "alias" => "god",
            "email" => "admin@mail.com",
            "password" => bcrypt("password"),
        ]);
        $rumpel->addRole("admin");

        $jeffrey = User::create([
            "alias" => "yerffej",
            "email" => "jeffrey@mail.com",
            "password" => bcrypt("cat"),
        ]);


        factory(User::class, 30)->create();

        $groups = factory(Group::class, 3)
            ->create()
            ->each(function (Group $group) use ($rumpel) {

                $users = User::where("id", ">", "2")->get();

                $group->members()->save(Member::create([
                    'group_id' => $group->id,
                    'user_id' => $rumpel->id,
                    'credits' => rand(0,10)
                ]));

                for ($i = 0; $i <= rand(4, 6); $i++) {
                    $key = rand(0, count($users) - 3);
                    $user = $users->random(1)->first();

                    $group->addMemberByUser($user);
                    $users->forget($key);
                }
                $group->members->each(function (Member $member){
                    $member->addRole("member");
                });

                $group->members->random(1)->first()->addRole("group-admin");

                $group->taskEvents()->saveMany(factory(TaskEvent::class, 4)->create([
                    "group_id" => $group->id,
                ])
                    ->each(function (TaskEvent $taskEvent) {

                        //FREQUENCY


                        if (rand(0, 3) > 1) {

                            $taskEvent->condition()->save(factory(Condition::class)->make([
                            ]));
                            $taskEvent->tags()->save(TaskTag::make([
                                "name" => TaskTag::getTag('condition'),
                            ]));

                            $taskEvent->frequency()->save(factory(Frequency::class)->make([
                            ]));
                            $taskEvent->tags()->save(TaskTag::make([
                                "name" => TaskTag::getTag('frequency'),
                            ]));

                            if (rand(0, 2) == 1) {
                                $taskEvent->expiration()->save(factory(Expiration::class)->make([

                                ]));

                                if (Carbon::parse($taskEvent->condition->time)->gte(Carbon::parse($taskEvent->expiration->time))) {
                                    $taskEvent->expiration->days += 1;
                                    $taskEvent->save();
                                }
                                $taskEvent->tags()->save(TaskTag::make([
                                    "name" => TaskTag::getTag('expiration'),
                                ]));
                            }
                        }
                        $taskEvent->update();

                        //TASK ORDER
                        $counter = -1;
                        foreach ($taskEvent->group->members as $member) {
                            $counter += 1;
                            $taskEvent->taskOrders()->save(TaskOrder::create([
                                "task_event_id" => $taskEvent->id,
                                "index" => $counter,
                                "member_id" => $member->id,
                            ]));
                        }
                        $taskEvent->update([
                            "order_index" => rand(0, count($taskEvent->group->members->all()) - 1)
                        ]);
                    }));



            });
        try{
            Group::find(1)->addMemberByUser($jeffrey);
        }catch(\Exception $e){
            dump($e->getMessage());
        }

        $hengelore = User::create([
            "alias" => "lorylore",
            "email" => "hengelore@mail.com",
            "password" => bcrypt("katzen"),
        ]);


        Artisan::call("passport:client",["--password"=>true]);

        $newCLient = DB::table("oauth_clients")->get()->last();

        echo "Client's secret: $newCLient->secret\nThe ID is '$newCLient->id'' \n";

    }
}

<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Peon\TaskEvent::class, function (Faker $faker) {


    $emojis = ["⚽","🐻","🐶","🐬","🐟","🍀","🚗","🍎","💝","💩","🍸","💖","🌟","🎉","🌺","👠","🏈","🏆","👽","💀","🐵","🐮","🐩","🐎","💣","🍓","💘","💜","🚽","💃","💎","🚀","🌙","🎁","⛄","🌊","⛵","🏀","🎱","💰","🐰","🐷","🐍","🐫","🔫","👄","🚲","🍉"];

    return [
        'title' => $emojis[$faker->numberBetween(0,count($emojis)-1)],
        'description' => $faker->paragraph,
        'credits' => $faker->numberBetween(0,10),
        'color' => sprintf("#%06x",rand(0,16777215)),

    ];
});

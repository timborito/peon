<?php

use Faker\Generator as Faker;



$factory->define(Peon\Frequency::class, function (Faker $faker) {

    $days = (rand(0,3)>0) ? $faker->numberBetween(1,3) : 0;
    $weeks = ($days == 0 && rand(0,1)==1) ? $faker->numberBetween(1,2) : 0;
    $months = ($days == 0 && $weeks == 0) ?  $faker->numberBetween(1,2) : 0;

    return [
        "day" => $days,
        "week" => $weeks,
        "month" => $months
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(Peon\Condition::class, function (Faker $faker) {
    return [
        "date" =>   $faker->dateTimeThisMonth($max = 'now', $timezone = null)->format('Y-m-d'),
        "time" =>   $faker->time(),
    ];
});

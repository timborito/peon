<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->text("message");
            $table->integer("credits");
            $table->unsignedInteger("member_reciever_id");
            $table->unsignedInteger("member_donator_id");
            $table->foreign("member_reciever_id")->references("id")->on("members");
            $table->foreign("member_donator_id")->references("id")->on("members");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "DOWN DONATIONS";
        Schema::dropIfExists('donations');
    }
}

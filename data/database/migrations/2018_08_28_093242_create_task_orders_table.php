<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("index");

            $table->unsignedInteger("task_event_id");
            $table->foreign("task_event_id")->references('id')->on("task_events")->onDelete('cascade');

            $table->unsignedInteger("member_id")->nullable();
            $table->foreign("member_id")->references('id')->on("members");


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "DOWN TASK ORDERS";
        Schema::dropIfExists('task_orders');
    }
}

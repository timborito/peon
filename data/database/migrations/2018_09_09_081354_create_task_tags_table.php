<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("task_event_id");
            $table->foreign("task_event_id")->references('id')->on("task_events")->onDelete('cascade');
            $table->string("name");
            $table->unique(["task_event_id","name"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "DOWN TASK TAGS";
        Schema::dropIfExists('task_tags');
    }
}

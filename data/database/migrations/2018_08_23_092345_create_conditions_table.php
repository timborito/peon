<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date("date");
            $table->time("time");

            $table->unsignedInteger("task_event_id");
            $table->foreign("task_event_id")->references("id")->on("task_events")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "DOWN CONDITIONS";
        Schema::dropIfExists('conditions');
    }
}

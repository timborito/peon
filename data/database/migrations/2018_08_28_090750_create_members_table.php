<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("credits")->nullable();
            $table->unsignedInteger("group_id");
            $table->foreign("group_id")->references("id")->on("groups")->onDelete("cascade");
            $table->unsignedInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "WHAT?!";
        Schema::dropIfExists('bookmarks');
        Schema::dropIfExists('member_roles');
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('task_orders');
        Schema::dropIfExists('donations');

        echo "DOWN MEMBERS";
        Schema::dropIfExists('members');
    }
}

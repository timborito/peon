<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("task_event_id");
            $table->foreign("task_event_id")->references('id')->on("task_events")->onDelete('cascade');

            $table->dateTime("launches_at")->nullable();
            $table->dateTime("expires_at")->nullable();
            $table->dateTime("processed_at")->nullable();
            $table->dateTime("expired_at")->nullable();

            $table->unsignedInteger("member_id");
            $table->foreign("member_id")->references('id')->on("members")->onDelete('cascade');

            $table->unsignedInteger("group_id");
            $table->foreign("group_id")->references('id')->on("groups")->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "DOWN TASKS";
        Schema::dropIfExists('tasks');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpirationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expirations', function (Blueprint $table) {
            $table->increments('id');
            $table->time("expires");
            $table->integer("days")->default(0);
            $table->unsignedInteger("task_event_id");
            $table->foreign("task_event_id")->references("id")->on("task_events")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "DOWN EXPIRATIONS";
        Schema::dropIfExists('expirations');
    }

}

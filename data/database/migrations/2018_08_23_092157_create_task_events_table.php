<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title");
            $table->text("description");
            $table->string("color");
            $table->integer("credits")->default(0);
            $table->integer("order_index")->default(0);
            $table->boolean("quit_on_expiration")->default(true);
            $table->unsignedInteger("group_id");
            $table->foreign("group_id")->references("id")->on("groups")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        echo "DOWN TASKSEVENTS";

        Schema::dropIfExists('bookmarks');
        Schema::dropIfExists('task_tags');
        Schema::dropIfExists('task_orders');
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('frequencies');
        Schema::dropIfExists('conditions');
        Schema::dropIfExists('expirations');
        echo "ey hallo?";
        Schema::dropIfExists('task_events');
    }
}

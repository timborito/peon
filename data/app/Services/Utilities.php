<?php

namespace Peon\Services;

use \Carbon\Carbon;
use \Log;
use Illuminate\Database\Eloquent\Model;

class Utilities
{

    public static function dateOrNow($on): Carbon
    {
        if (!empty($on)) {
            return Carbon::parse($on);
        }
        return Carbon::now();
    }

    public static function setTimeFromString(Carbon $destination, String $source): Carbon
    {
        $source = Carbon::parse($source);
        return Utilities::setTimeFromDate($destination,$source);
    }

    public static function setTimeFromTime(Carbon $destination, Time $source): Carbon
    {
        $source = Carbon::parse($source);
        return Utilities::setTimeFromDate($destination,$source);
    }

    public static function setTimeFromDate(Carbon $destination, Carbon $source): Carbon
    {
        return $destination->setTime($source->hour, $source->minute, $source->second, $source->micro);
    }

    public static function getFillableArray(String $model)
    {
        $attributes = $model::make()->getFillable();
        foreach ($attributes as $key=>$val){
            $attributes[$val] = null;
            unset($attributes[$key]);
        }
        return $attributes;
    }



}
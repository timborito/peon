<?php
/**
 * Created by PhpStorm.
 * User: timborito
 * Date: 04.09.18
 * Time: 18:47
 */

namespace Peon\Services\TaskSchedule;

use Peon;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class Verification
{
    protected $schedule;

    public function __construct(Peon\Services\TaskSchedule $schedule)
    {
        $this->schedule = $schedule;
    }


    public function group(Peon\Group $group, Carbon $on = null)
    {

        $out = array();
        //verify all Peon\Group->taskEvents
        $group->taskEvents->each(function ($taskEvent) use ($on, &$out) {
                $out = array_merge($out, $this->taskEvent($taskEvent, $on));
        });


        return $out;
    }

    /**
     * @param Peon\TaskEvent $taskEvent
     * @param Carbon|null $on
     * @return array
     */
    public function taskEvent(Peon\TaskEvent $taskEvent, Carbon $reference = null)
    {
        $out = array();
        //no tasks orders - no members added to this task - nothing's to verify
        if(count($taskEvent->taskOrders)==0){
            return [];
        }
        //must reload Peon\TaskEvent->tasks because of recursive structure
        $taskEvent->load("tasks");
        //get all unfinished Peon\TaskEvent->tasks

        $incompleteTasks = $taskEvent->tasks->filter(function ($task){
            return ($task->processed_at == null && $task->expired_at == null);
        });




        if (count($incompleteTasks) == 0) {
            //there might be no Peon\TaskEvent->tasks
            if (count($taskEvent->tasks) == 0) {
                $taskEvent->load("condition");
                $task = $this->schedule->generateTask($taskEvent, $reference);
               /*
                if (!is_null($task)) {
                    Log::debug("verfiy");
                    $out[] = $this->task($task, $reference);
                }
               */
                $out = array_merge($out,$this->task($task, $reference));

            }
        } else {

            //verify Peon\Tasks
            $incompleteTasks->each(function ($task) use ($reference,&$out) {
                $out = array_merge($out,$this->task($task, $reference));
            });
        }
        return $out;
    }

    /**
     * @param Peon\Task $task
     * @param Carbon|null $reference
     * @return Peon\Task[]
     */
    public function task(Peon\Task $task, Carbon $reference = null)
    {
        $out = [];

        $dateVerify = Peon\Services\Utilities::dateOrNow($reference);

        if (is_null($task->expires_at)) {
            $out[] = $task;
            return $out;
        }
        if (Carbon::parse($task->expires_at)->lte($dateVerify)) {

            $task->expired_at = $dateVerify->format('Y-m-d H:i:s');
            $task->update();

            $out = array_merge($out,$this->schedule->proceed($task->taskEvent, $reference));
        }
        $out[] = $task;

        return $out;
    }
}
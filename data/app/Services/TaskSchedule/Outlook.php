<?php
/**
 * Created by PhpStorm.
 * User: timborito
 * Date: 06.09.18
 * Time: 00:40
 */

namespace Peon\Services\TaskSchedule;

use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Peon;
use Peon\Bookmark;
use Peon\Services\Utilities;


class Outlook
{

    protected $schedule;

    public function __construct(Peon\Services\TaskSchedule $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     * @param Peon\Group $group
     * @param Carbon|null $until
     * @return \Illuminate\Support\Collection
     */
    public function group(Peon\Group $group, Carbon $until = null, Carbon $reference = null)
    {
        $tasks = collect([]);
        $group->load('taskEvents');
        $group->taskEvents->each(function (Peon\TaskEvent $taskEvent) use ($until, &$tasks, $reference) {
            foreach($this->taskEvent($taskEvent,$until,$reference) as $outlook){
                $tasks->push($outlook);
            };
        });

        return $tasks;
    }

    public function taskEvent(Peon\TaskEvent $taskEvent, Carbon $until = null, Carbon $reference = null)
    {
        $tasks = collect([]);
        $taskEvent->load('tasks');
        $taskEvent->tasks->filter(function ($task){
            return ($task->processed_at == null && $task->expired_at == null);
        })

            ->each(
                function (Peon\Task $task) use ($until, &$tasks, &$taskEvent, $reference) {


                    if (is_null($taskEvent->frequency)) {
                        return;
                    }

                    $iteration_distance = 0;
                    while ($taskEvent->condition->getDate($reference)->gt(Carbon::parse($until)->addDay()) == false) {
                        // iterate worker
                        $taskEvent->order_index += 1;
                        if ($taskEvent->order_index > $taskEvent->taskOrders->last()->index) {
                            $taskEvent->order_index = 0;
                        }
                        //iterate condition with frequency
                        if (!is_null($taskEvent->condition) && !is_null($taskEvent->frequency)) {

                            $conditionDate = $taskEvent->getConditionDate($reference);
                            $conditionDate = $taskEvent->frequency->calc($conditionDate);
                            if ($taskEvent->condition->date) {
                                $taskEvent->condition->date = $conditionDate->format('Y-m-d');
                            }
                            if ($taskEvent->condition->time) {
                                $taskEvent->condition->time = $conditionDate->format('H:i:s');
                            }
                        }
                        $new = $this->schedule->generateTask($taskEvent, $reference, false);

                        if ($taskEvent->condition) {

                            $new->launches_at = $taskEvent->condition->getDate($reference)->format('Y-m-d H:i:s');
                        }else{
                            $new->launches_at = $reference;
                        }

                        $iteration_distance += 1;

                        $new->outlook = true;
                        $new->taskEvent = $taskEvent;
                        $new->iteration_distance = $iteration_distance;


                        $bookmark = Bookmark::where("iteration_distance",$iteration_distance)->where("task_event_id",$taskEvent->id)->first();
                        if($bookmark){
                            $new->bookmark = true;
                            $new->member_id = $bookmark->member_id;
                        }


                        if ($taskEvent->condition->getDate()->gt(Carbon::parse($until)->addDay())) {
                            return;
                        }
                        $tasks->push($new);

                    }

                });

            return $tasks;

    }

}
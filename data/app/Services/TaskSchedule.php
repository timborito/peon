<?php

namespace Peon\Services;

use Peon;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Peon\Bookmark;
use Peon\Member;
use Peon\Task;

class TaskSchedule
{
    /** @var Peon\Services\TaskSchedule\Verification */
    protected $verfication;
    /** @var Peon\Services\TaskSchedule\Outlook */
    protected $outlook;

    public function __construct()
    {
        $this->verfication = new TaskSchedule\Verification($this);
        $this->outlook = new TaskSchedule\Outlook($this);
    }

    public function verify(): Peon\Services\TaskSchedule\Verification
    {
        return $this->verfication;
    }

    public function outlook(): Peon\Services\TaskSchedule\Outlook
    {
        return $this->outlook;
    }


    /**
     * @param Peon\TaskEvent $taskEvent
     * @param Carbon|null $reference
     * @return Peon\Task[]
     */
    public function proceed(Peon\TaskEvent $taskEvent, Carbon $reference = null)
    {

        $bookmarkedIndex = null;

        Bookmark::where("task_event_id", $taskEvent->id)->each(function (Bookmark $bookmark) use ($taskEvent, &$bookmarkedIndex) {
            $bookmark->iteration_distance -= 1;
            if ($bookmark->iteration_distance == 0) {
                $taskOrder = $taskEvent->taskOrders->where("member_id", $bookmark->member)->first();
                if ($taskOrder) {
                    $bookmarkedIndex = $taskOrder->index;
                }
                $bookmark->delete();
            } else {
                $bookmark->update();
            }

        });

        if ($bookmarkedIndex) {
            $taskEvent->update(["order_index" => $bookmarkedIndex]);
        } else {
            // iterate worker
            $attr = ["order_index" => $taskEvent->order_index + 1];
            if ($attr["order_index"] > $taskEvent->taskOrders->last()->index) {
                $attr["order_index"] = 0;
            }
            $taskEvent->update($attr);
        }

        //iterate condition with frequency
        if ($taskEvent->condition && $taskEvent->frequency) {
            $conditionDate = $taskEvent->getConditionDate($reference);
            $conditionDate = $taskEvent->frequency->calc($conditionDate);
            $attr = array();
            if ($taskEvent->condition->date) {
                $attr["date"] = $conditionDate->format('Y-m-d');
            }
            if ($taskEvent->condition->time) {
                $attr["time"] = $conditionDate->format('H:i:s');
            }
            $taskEvent->condition->update($attr);
        }

        $taskEvent->update();
        $taskEvent->load('condition');

        $task = $this->generateTask($taskEvent, $reference);
        $out = $this->verify()->task($task, $reference);
        return $out;
    }

    /**
     * @param Peon\TaskEvent $taskEvent
     * @param bool $initial
     * @param Carbon|null $reference
     * @return Peon\Task
     */
    public function generateTask(Peon\TaskEvent $taskEvent, Carbon $reference = null, $persist = true)
    {

        if(count($taskEvent->taskOrders)==0){
            return null;
        }

        $referenceDate = Utilities::dateOrNow($reference);

        try{
            $taskEvent->getOrderMember()->id;
        }catch(\Exception $e){
            dd($taskEvent->taskOrders);
        }

        $attr = Utilities::getFillableArray(Peon\Task::class);
        $attr = array_merge($attr, [
            "task_event_id" => $taskEvent->id,
            "member_id" => $taskEvent->getOrderMember()->id,
            "group_id" => $taskEvent->group->id,
        ]);


        if ($taskEvent->condition) {
            $attr["launches_at"] = $taskEvent->condition->getDate($reference);
        } else {
            $attr["launches_at"] = $referenceDate;
        }
        //set expires_at
        if (!is_null($taskEvent->expiration)) {
            if (!is_null($taskEvent->condition)) {
                $attr["expires_at"] = $taskEvent->expiration->getDate($taskEvent->condition->getDate($referenceDate))->format('Y-m-d H:i:s');
            } else {
                $attr["expires_at"] = $taskEvent->expiration->getDate($referenceDate)->format('Y-m-d H:i:s');
            }
        } else if ($taskEvent->frequency) {
            $attr["expires_at"] = $taskEvent->frequency->calc($taskEvent->condition->getDate($referenceDate))->format('Y-m-d H:i:s');
        }


        //CREATE TASK
        $task = Peon\Task::make($attr);
        if ($persist) {
            $task->save();
            $task = Task::where("task_event_id",$task->task_event_id)->get()->last();
        }
        return $task;
    }

    /**
     * @param Peon\Task $task
     * @param Carbon|null $reference
     * @throws \Exception
     */
    public function finish(Peon\Task $task, Carbon $reference = null, Member $member)
    {
        if ($task->processed_at || $task->expired_at) {
            return false;
        }
        $referenceDate = Utilities::dateOrNow($reference);
        $taskEvent = $task->taskEvent;
        if ($taskEvent->condition) {
            if ($referenceDate->lt($taskEvent->condition->getDate($reference))) {
                throw new \Exception("Reference date does not fulfill the condition of this TaskEvent");
            }
        }
        $worker = $task->taskEvent->getOrderMember();
        if ($member->id == $worker->id) {
            $worker->update([
                "credits" => $worker->credits + $taskEvent->credits,
            ]);
        }
        $task->processed_at = ($referenceDate)->format('Y-m-d H:i:s');
        $task->save();
        $this->proceed($task->taskEvent, $reference);
        return true;
    }

    /**
     * @param Peon\TaskEvent $task
     * @param Carbon|null $reference
     * @throws \Exception
     */
    public function finishRunning(Peon\TaskEvent $taskEvent, Carbon $reference = null, Member $member)
    {
        $task = $taskEvent->tasks->filter(function ($task) {
            return ($task->processed_at == null && $task->expired_at == null);
        })->first();
        if (is_null($task)) {
            return "null";
        }
        $this->finish($task, $reference, $member);
        return $task;
    }
}
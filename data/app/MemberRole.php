<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;
use Peon\Role;

class MemberRole extends Model
{
    protected $fillable = [
        "member_id",
        "role_id",
    ];
    public function member(){
        return $this->belongsTo("Peon\Member");
    }
    public function role(){
        return $this->belongsTo("Peon\Role");
    }
}

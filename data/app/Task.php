<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Peon\Services\Utilities;
use Validator;


class Task extends Model
{




    protected $fillable = [
        "task_event_id",
        "launches_at",
        "expires_at",
        "processed_at",
        "expired_at",
        "member_id",
        "group_id",
    ];



    public function taskEvent()
    {
        return $this->belongsTo("Peon\TaskEvent");
    }

    public function group()
    {
        return $this->belongsTo("Peon\Group");

    }

    public function member()
    {
        return $this->belongsTo("Peon\Member");
    }

    /**
     * @param array $attriutes
     * @return \Illuminate\Validation\Validator
     */
    public static function validation(array $attriutes){
        $validator = Validator::make($attriutes, [
            "processed_at" => "date_format:Y-m-d H:i:s",
            "launches_at" => "date_format:Y-m-d H:i:s",
            "expires_at" => "date_format:Y-m-d H:i:s",
            "expired_at" => "date_format:Y-m-d H:i:s",
            "member_id" => "integer",
            "task_event_id" => "integer",
        ]);
        return $validator;
    }



}

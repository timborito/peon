<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{

    protected $fillable = [
        "iteration_distance" ,
        "task_event_id" ,
        "member_id" ,
    ];

    /**
     * @param array $attributes
     * @return \Illuminate\Validation\Validator
     */
    public static function validation(array $attributes){
        $validator =  Validator::make($attributes, [
            "iteration_distance" => "integer|required",
            "task_event_id" => "integer|required",
            "member_id" => "integer",
        ]);
        return $validator;
    }

    public function member(){
        return $this->belongsTo("Peon\Member");
    }

}

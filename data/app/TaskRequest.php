<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;

class TaskRequest extends Model
{
    //

    public function task(){
        return $this->belongsTo("Peon\Task");
    }

    public function bearer()
    {
        return $this->hasOne("Peon\Member", "member_bearer_id");
    }

    public function initiator()
    {
        return $this->hasOne("Peon\Member", "member_initiator_id");
    }


}

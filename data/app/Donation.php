<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    //
    public function donator(){
        return $this->hasOne("Peon\Member","member_donator_id");
    }
    public function reciever(){
        return $this->hasOne("Peon\Member","member_reciever_id");
    }
}

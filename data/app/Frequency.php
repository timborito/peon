<?php

namespace Peon;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Validator;

class Frequency extends Model
{
    protected $fillable = [
        "day",
        "week",
        "month",
    ];

    public function calc(Carbon $date): Carbon
    {

        $return = Carbon::parse($date);
        if($this->day>0){
            $return->addDays($this->day);
        } else if($this->week>0){
            $return->addWeek($this->week);
        } else {
            $return->addMonth($this->month);
        }

        return $return;
    }


    /**
     * @param array $attributes
     * @return \Illuminate\Validation\Validator
     */
    public static function validation(array $attributes){

        $validator = Validator::make($attributes, [
            "day" => 'integer',
            "week" => 'integer',
            "month" => 'integer'
        ]);
        return $validator;
    }
}

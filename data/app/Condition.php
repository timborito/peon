<?php

namespace Peon;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Peon\Services\Utilities;
use Validator;

class Condition extends Model
{

    protected $fillable = [
        "date",
        "time",
        "task_event_id",
    ];

    public function taskEvent()
    {
        return $this->belongsTo("Peon\TaskEvent");
    }

    public function getDate($reference = null): Carbon
    {
        $date = null;
        if($this->time && is_null($this->date)){
            $date = Utilities::setTimeFromString($reference, $this->time);
        }else {
            $date = Carbon::parse($this->date);
            if ($this->time) {
                $date = Utilities::setTimeFromString($date, $this->time);
            }

        }
        return $date;
    }

    /**
     * @param array $attributes
     * @return \Illuminate\Validation\Validator
     */
    public static function validation(array $attributes){
        $validator =  Validator::make($attributes, [
            "date" => "date_format:Y-m-d|required_without:time",
            "time" => "date_format:H:i|required_without:date",
        ]);
        return $validator;
    }
}

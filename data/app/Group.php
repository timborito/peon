<?php

namespace Peon;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Log;
use Validator;
use Peon\Services\Utilities;
use Tests\CreatesApplication;

class Group extends Model
{
    //
    protected $fillable = [
        "title",
        "creditlimit"
    ];

    /**
     * @param array $attributes
     * @return \Illuminate\Validation\Validator
     */
    public static function validation(array $attributes)
    {
        $validator = Validator::make($attributes, Group::rules());
        return $validator;
    }

    public static function rules()
    {
        return [
            'title' => 'String|min:1',
            'creditlimit' => 'integer|min:1',
        ];
    }

    public function members()
    {
        return $this->hasMany("Peon\Member");

    }

    public function taskEvents()
    {
        return $this->hasMany("Peon\TaskEvent");
    }

    public function tasks()
    {
        return $this->hasMany("Peon\Task");
    }

    public function delete()
    {

        if($this->taskEvents){
            $this->taskEvents->each(function (TaskEvent $taskEvent){
                $taskEvent->delete();
            });
        }
        if($this->members){
            $this->members->each(function (Member $member){
                $member->delete();
            });
        }

        return parent::delete();
    }

    public function addMemberByUser(User $user){
        $member = $this->members()->save(Member::make([
            "user_id" => $user->id,
            "group_id" => $this->id,
        ]));

        $member->addRole("member");
        $member->update();
    }


}

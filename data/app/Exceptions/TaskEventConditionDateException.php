<?php

namespace Peon\Exceptions;

use Exception;

class TaskEventConditionDateException extends Exception
{
    //

    public function report(){
        \Log::debug("Reference date does not fulfill the condition of this TaskEvent");
    }
}

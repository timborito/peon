<?php

namespace Peon\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('Peon\Services\TaskSchedule', function () {
            return new \Peon\Services\TaskSchedule();
        });
    }
}

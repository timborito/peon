<?php

namespace Peon\Providers;

use function foo\func;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

use Peon;
use Peon\Group;
use Peon\Member;
use Peon\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Gate::define("admin-group",function ($user,$group){

            if($user->hasRole("admin")){
                return true;
            }
            $member = Member::getMemberByUserInGroup( $user,$group);
            if(is_null($member)){
                return false;
            }
            if($member->hasRole('group-admin')){
                return true;
            };
            return false;
        });

        Gate::define("use-group",function ($user,$group){

            if($user->hasRole("admin")){
                return true;
            }

            $member = Member::getMemberByUserInGroup($user,$group);
            if(is_null($member)){
                return false;
            }
            if($member->hasRole('member')){
                return true;
            };
            return false;
        });

        Gate::define("use-member",function ($user,$group,$member){

            if($user->hasRole("admin")){
                return true;
            }

            if(Gate::denies("use-group",[$user,$group])==false){
                return false;
            }

            $memberByUser = Member::getMemberByUserInGroup($user,$group);

            if(is_null($memberByUser)){
                return false;
            }
            if($memberByUser->id != $member->id){
                return false;
            }
            return false;
        });

        Gate::define("admin",function ($user,$group){
            if($user->hasRole('admin')){
                return true;
            }
            return false;
        });

        //
    }
}

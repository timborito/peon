<?php

namespace Peon;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Peon\Permissions\HasPermissions;
use Peon\Permissions\HasRoles;


class User extends Authenticatable implements HasRoles
{
    use HasPermissions,HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'alias', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function getRoleIdentifier()
    {
        return "\Peon\UserRole";
    }

    function getIdentifier()
    {
        return "user_id";
    }
}

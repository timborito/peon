<?php
namespace Peon\Permissions;

interface HasRoles{
    function getRoleIdentifier();
    function getIdentifier();
}
<?php

namespace Peon\Permissions;

use Peon\MemberRole;
use Peon\Permission;
use Peon\Role;

trait HasPermissions
{

    public function roles()
    {
        return $this->hasMany($this->getRoleIdentifier() );
    }

    // "..." = splat operator

    public function addRole(String $slug)
    {
        $role = Role::getBySlug($slug);
        if(is_null($role)){
            return false;
        }

        $relation = $this->getRoleIdentifier()::make([
            $this->getIdentifier() => $this->id,
            "role_id" => $role->id,
        ]);
        $this->roles()->save($relation);
        $this->save();
        return true;
    }


    public function hasRole(... $roles)
    {
        $hasRole = false;
        foreach ($roles as $role) {
            $this->roles->each(function ($mRole) use ($role,&$hasRole) {
                if ($mRole->role->slug == $role) {
                    $hasRole = true;
                    return ;
                }
            });
        }
        return $hasRole;
    }
}
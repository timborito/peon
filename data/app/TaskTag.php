<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;

class TaskTag extends Model
{

    protected $fillable = [
        "task_event_id",
        "name",
    ];

    public function taskEvent(){
        return $this->belongsTo('Peon\TaskEvent');
    }


    public static function tags()
    {
        return [
            "condition" => "🔫",
            "expiration" => "🏁",
            "frequency" => "⏰",
            "outlook" => "🔭",
        ];


    }

    /**
     * @param String $tag
     * @return null|String
     * @throws \Exception
     */

    public static function getTag(String $tag){
        if(array_key_exists($tag,TaskTag::tags())){
            return $tag;
        }
        throw new \Exception("Tag does not exsits.");
    }
}

<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Log;
use Peon\Services\Utilities;
use Validator;
class Expiration extends Model
{
    protected $fillable = [
        "expires",
        "days",
        "task_event_id",
    ];

    public function taskEvent()
    {
        return $this->belongsTo("Peon\TaskEvent");
    }

    public function getDate($reference = null): Carbon
    {
        $date = Utilities::dateOrNow($reference);
        $time = Carbon::parse($this->expires);
        $date = Carbon::parse($date);
        $date->addDay($this->days);
        $date = Utilities::setTimeFromDate($date, $time);
        return $date;
    }
    /**
     * @param array $attributes
     * @return \Illuminate\Validation\Validator
     */
    public static function validation(array $attributes){
        $validator =  Validator::make($attributes, [
            "expires" => "required|date_format:H:i",
            "days" => "integer",
        ]);
        return $validator;
    }
}

<?php

namespace Peon\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "title" => mb_strimwidth($this->title,0,env('UI_STRING_MAX_LENGTH'), "...")
        ];
    }
}

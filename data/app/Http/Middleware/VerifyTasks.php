<?php

namespace Peon\Http\Middleware;

use Closure;
use App;

class VerifyTasks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $schedule = App::make("Peon\Services\TaskSchedule");
        if ($group = $request->route("group")) {
            $verification = $schedule->verify()->group($group);
        } else if ($taskEvent = $request->route("taskEvent")) {
            $verification = $schedule->verify()->taskEvent($taskEvent);
        } else if ($task = $request->route("task")) {
            $verification = $schedule->verify()->task($task);
        }


        $request->merge(compact('verification'));
        //dd($verification);
        return $next($request);
    }
}

<?php

namespace Peon\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Peon\Exceptions\TaskEventConditionDateException;
use Peon\Member;
use Peon\Task;
use Peon\Group;
use Peon\TaskEvent;
use Validator;
use Carbon\Carbon;
use App;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * @return \Peon\Services\TaskSchedule
     */
    public function taskSchedule()
    {
        return App::make("Peon\Services\TaskSchedule");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group, TaskEvent $taskEvent)
    {
        $user = auth()->user();
        if(Gate::denies("admin",[$group,$user])){
            abort(401);
        }
        return response()->json($taskEvent->tasks, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        if(Gate::denies("admin",[$group,$user])){
            abort(401);
        }
        return response()->json("create", 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group, TaskEvent $taskEvent)
    {
        $user = auth()->user();
        if(Gate::denies("admin",[$group,$user])){
            abort(401);
        }
        $validator = Task::validation($request->all());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            $attr = $request->all();
            $attr["member_id"] = $taskEvent->getOrderMember()->id;
            $attr["group_id"] = $group->id;
            $task = $taskEvent->tasks()->save(Task::make($attr));
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
        return response()->json($task, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Peon\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group, TaskEvent $taskEvent,Task $task)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json($task, 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Peon\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group, TaskEvent $taskEvent,Task $task)
    {
        $user = auth()->user();
        if(Gate::denies("admin",[$group,$user])){
            abort(401);
        }
        return response()->json("edit", 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Peon\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group, TaskEvent $taskEvent, Task $task)
    {
        $user = auth()->user();
        if(Gate::denies("admin",[$group,$user])){
            abort(401);
        }
        $validator = Task::validation($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);

        }
        $update = $task->update($request->all());
        return response()->json([$update,$task], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Peon\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Group $group, TaskEvent $taskEvent,Task $task)
    {
        $user = auth()->user();
        if(Gate::denies("admin",[$group,$user])){
            abort(401);
        }
        try {
            $taskEvent->delete();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
        return response()->json($task, 200);
    }

    public function finish(Request $request, Group $group, TaskEvent $taskEvent,Task $task){
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        $date = null;
        $validator = Validator::make($request->all(), [
            'on' => '',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            $date = Carbon::parse($request->get("on"));
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
        $member = Member::getMemberByUserInGroup($user,$group);
        try {
            $finish = $this->taskSchedule()->finish($task, $date,$member);
            return response()->json([$finish,$task], 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function bookmark(Request $request, Group $group, TaskEvent $taskEvent,Task $task){
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
    }
}

<?php

namespace Peon\Http\Controllers;

use \Carbon\Carbon;
use function foo\func;
use Illuminate\Support\Facades\Gate;
use Peon\Group;
use Illuminate\Http\Request;
use Peon\Services\Utilities;
use Peon\TaskEvent;
use Peon\TaskTag;
use Peon\Member;
use Validator;
use Illuminate\Support\Facades\Log;
use App;
use Calendar;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('verify.tasks', ['except' => ['index', 'create', 'store', 'verify']]);
        $this->middleware('locale');
    }

    /**
     * @return \Peon\Services\TaskSchedule
     */
    public function taskSchedule()
    {
        return App::make("Peon\Services\TaskSchedule");
    }

    public function index()
    {
        return response()->json(Group::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json("create", 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $validator = Group::validation($request->all());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $group = \Peon\Group::create($request->all());
        $userID = $user->id;
        if($request->has("user_id")){
            $userID = $request->get("user_id");
        }
        $member = Member::make([
            "user_id" => $userID
        ]);
        $group->members()->save($member);
        $member->addRole("group-admin");
        $group->update();
        return response()->json($group, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Peon\Group $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $user = auth()->user();

        if(Gate::denies("use-group",[$group,$user])){
            ;
            abort(401);
        }
        return response()->json($group, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Peon\Group $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        $events = [];
        $until = null;
        //get tasks
        $tasks = $group->tasks;
        //validate request
        $validator = Validator::make($request->all(), array_merge([
            'outlook' => 'string',
        ], Group::rules()));

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            if ($request->get("outlook")) {
                $until = Carbon::parse(urldecode($request->get("outlook")));
            }
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
        // add outlook
        if (!is_null($until)) {
            foreach ($this->taskSchedule()->outlook()->group($group, $until) as $outlook) {
                $tasks->push($outlook);
            }
        }

        //create Events for Fullcalendar for each Task
        $tasks->each(function ($task) use (&$events) {
            $color = $displayTime = $displayEnd = null;
            $fullDay = false;

            if ($task->taskEvent->expiration) {
                $endDate = Carbon::parse($task->expires_at)->format('Y-m-d H:i:s');;
                $displayEnd = " - " . Carbon::parse($task->expires_at)->format('H:i');
            } else {
                $endDate = Carbon::parse($task->launches_at)->addDay()->setTime(0, 0, 0, 0)->format('Y-m-d H:i:s');;
                $fullDay = true;
            }

            if ($task->taskEvent->condition) {
                $displayTime = Carbon::parse($task->launches_at)->format('H:i');
            }


            if (($task->processed_at) || ($task->expired_at)) {

                $color = '#666666';
            } else if ($task->outlook && !($task->bookmark)) {
                list($r, $g, $b) = sscanf($task->taskEvent->color, "#%02x%02x%02x");
                $color = "rgba($r, $g, $b,0.3)";
            } else {
                $color = $task->taskEvent->color;
            }

            $tags = "";


            $task->taskEvent->tags->each(function ($tag) use (&$tags) {
                if (strlen($tags) != 0) {
                    $tags .= "  ";
                }
                $tags .= TaskTag::tags()[$tag->name];

            });

            $events[] = Calendar::event(
                (($task->bookmark)?"📖":"").
                "$displayTime $displayEnd \n" .
                "#" . $task->taskEvent->id . " " . $task->taskEvent->title
                . " | $tags"
                . "\n" . \Peon\Member::find($task->member_id)->user->alias,
                false,
                $task->launches_at,
                $endDate,
                null,
                // Add color and link on event
                [
                    'color' => $color,
                    'url' => 'pass here url and any route',
                ]
            );
        });


        $calendar = Calendar::addEvents($events)
            ->setOptions([
                "aspectRatio" => 0.7,
                "locale" => App::getLocale(),
                "displayEventTime" => false,
                "eventLimit" => false,
            ])
            ->setCallbacks([
                "dayRender" => 'function (date, cell) {
                            var string = "' . $until . '";
                            var day = jQuery.fullCalendar.formatDate(date,"Y-MM-DD");
                            var check = jQuery.fullCalendar.moment(string);
                            check = jQuery.fullCalendar.formatDate(check,"Y-MM-DD");
            
                            console.log(string);
                            console.log(day);
                            console.log(check);
                    
    
                                if(day == check){
                                    cell.css({
                                    "background-color": "rgba(255, 99, 71,0.5)",
                                    });
                                }
                                
                            }'
            ]);
        return view('fullcalendar', compact('calendar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Peon\Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        $validator = Group::validation($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);

        }
        $update = $group->update($request->all());
        return response()->json([$update,$group], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Peon\Group $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        try {
            $group->delete();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
        return response()->json($group, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Peon\Group $group
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request, Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        $date = null;
        $validator = Validator::make($request->all(), [
            'on' => '',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            $date = Carbon::parse($request->get("on"));
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }

        $output = $this->taskSchedule()->verify()->group($group, $date);
        return response()->json($output, 200);
    }

    public function outlook(Request $request, Group $group){
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        $tasks = collect([]);
        $validator = Validator::make($request->all(), [
            'until' => '',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            if ($request->get("until")) {
                $until = Carbon::parse(urldecode($request->get("until")));
            }
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
        // add outlook
        if (!is_null($until)) {
            foreach ($this->taskSchedule()->outlook()->group($group, $until) as $outlook) {
                $tasks->push($outlook);
            }
            return response()->json($tasks, 200);
        }else{
            return response()->json("Field 'until' must be set.", 422);
        }
    }


}

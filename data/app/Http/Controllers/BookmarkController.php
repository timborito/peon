<?php

namespace Peon\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Peon\Bookmark;
use Illuminate\Http\Request;
use Peon\Group;
use Peon\Services\Utilities;
use Peon\TaskEvent;
use Peon\Member;

class BookmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group, TaskEvent $taskEvent)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json($taskEvent->bookmarks, 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Group $group, TaskEvent $taskEven, Bookmark $bookmark)
    {

        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json("create", 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group, TaskEvent $taskEvent)
    {

        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        $validator = Bookmark::validation($request->all());

        if (count($validator->errors()) > 0) {
            return response()->json($validator->errors(), 422);
        }

        $attr = $request->all();

        if(is_null($attr["member_id"])){
            $member = Member::getMemberByUserInGroup($user,$group);
            $attr["member_id"] = $member->id;
        }

        $bookmark = Bookmark::make($request->all());
        $taskEvent->bookmarks()->save($bookmark);

        return response()->json($bookmark, 201);
    }



    /**
     * Display the specified resource.
     *
     * @param  \Peon\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group, TaskEvent $taskEven, Bookmark $bookmark)
    {

        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json($bookmark, 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Peon\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group, TaskEvent $taskEven, Bookmark $bookmark)
    {

        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json("edit", 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Peon\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group, TaskEvent $taskEvent,Bookmark $bookmark)
    {
        $user = auth()->user();
        if(Gate::denies("admin",[$group,$user])){
            abort(401);
        }
        $validator = Bookmark::validation($request->all());

        if (count($validator->errors()) > 0) {
            return response()->json($validator->errors(), 422);
        }

        $bookmark->update($request->all());

        return response()->json($bookmark, 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Peon\Bookmark  $bookmark
     * @return \Illuminate\Http\Response
     */
    public function destroy( Group $group, TaskEvent $taskEvent,Bookmark $bookmark)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        $member = Member::getMemberByUserInGroup($user,$group);
        if($member->id != $bookmark->member->id){
            abort(401);
        }
        if($bookmark->correspond){
            $bookmark->correspond->delte();
        }
        $bookmark->delte();
        return response()->json($bookmark, 200);
    }
}

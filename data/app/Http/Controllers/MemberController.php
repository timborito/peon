<?php

namespace Peon\Http\Controllers;

use App;
use Illuminate\Support\Facades\Gate;
use Peon\Member;
use Peon\Group;
use Peon\TaskOrder;
use Peon\Task;
use Peon\TaskEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;


class MemberController extends Controller
{

    public function __construct()
    {
        $this->middleware('verify.tasks', ['except' => ['index', 'create', 'store']]);
    }

    /**
     * @return \Peon\Services\TaskSchedule
     */
    public function taskSchedule()
    {
        return App::make("Peon\Services\TaskSchedule");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json($group->members, 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }

        return view('member.create', compact(''));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group)
    {

        $user = auth()->user();


        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }

        $validator = Validator::make($request->all(), [
            "user_id" => 'required',
            "credits" => "integer"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $member = Member::make($request->all());

        $group->taskEvents->each(function (TaskEvent $taskEvent) use($member){
            $taskEvent->load("taskOrders");

            $taskEvent->taskOrders()->save(TaskOrder::make([
                "index" => ($taskEvent->getNextIndex()),
                "member_id" => $member->id,
            ]));
        });

        $group->members()->save($member);
        $member->addRole("member");
        $member->save();



        return response()->json($member, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Peon\Member $member
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group, Member $member)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json($member, 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Peon\Member $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group, Member $member)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        return response()->json("edit", 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Peon\Member $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group, Member $member)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            "credits" => 'integer',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $update = $member->update($request->all());
        return response()->json([$update, $member], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Peon\Member $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group, Member $member)
    {

        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        try {
            $member->delete();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);

        }
        return response()->json($member, 200);
    }

    public function outlook(Request  $request, Group $group, Member $member)
    {
        $user = auth()->user();
        if(Gate::denies("use-member",[$group,$user,$member])){
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            'until' => '',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            if ($request->get("until")) {
                $until = Carbon::parse(urldecode($request->get("until")));
            }
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
        // add outlook
        if (!is_null($until)) {
            $tasks = collect([]);
            TaskOrder::where("member_id", $member->id)->each(function (TaskOrder $taskOrder) use ($until, &$tasks,$member) {
                foreach ($this->taskSchedule()->outlook()->taskEvent($taskOrder->taskEvent, $until) as $outlook) {
                    if($outlook->member_id == $member->id){
                        unset($outlook->taskEvent);
                        $tasks->push($outlook);
                    }
                }
            });
            return response()->json($tasks, 200);
        } else {
            return response()->json("Field 'until' must be set.", 422);
        }
    }

}

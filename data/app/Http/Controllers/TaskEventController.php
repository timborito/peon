<?php

namespace Peon\Http\Controllers;

use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Peon\Bookmark;
use Validator;
use Peon\Condition;
use Peon\Exceptions\TaskEventConditionDateException;
use Peon\Expiration;
use Peon\Frequency;
use Peon\Http\Requests\StoreTaskEvent;
use Peon\Member;
use Peon\Services\Utilities;
use Peon\Task;
use Peon\TaskEvent;
use Peon\TaskOrder;
use Peon\TaskTag;
use Peon\Group;




use Illuminate\Http\Request;

class TaskEventController extends Controller
{

    /**
     * @return \Peon\Services\TaskSchedule
     */
    public function taskSchedule()
    {
        return App::make("Peon\Services\TaskSchedule");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json($group->taskEvents, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        return response()->json("create", 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        $validator = TaskEvent::validation($request->all());

        if (count($validator->errors()) > 0) {
            return response()->json($validator->errors(), 422);
        }

        try {
            $taskEvent = TaskEvent::make($request->except(["condition", "expiration", "frequency"]));
            $group->taskEvents()->save($taskEvent);
            if ($request->get("condition")) {
                $taskEvent->condition()->save(Condition::make($request->get("condition")));
                $taskEvent->tags()->save(TaskTag::create([
                    "name" => TaskTag::getTag('condition'),
                    "task_event_id" => $taskEvent->id,
                ]));
            }
            if ($request->get("expiration")) {
                $taskEvent->expiration()->save(Expiration::make($request->get("expiration")));
                $taskEvent->tags()->save(TaskTag::create([
                    "name" => TaskTag::getTag('expiration'),
                    "task_event_id" => $taskEvent->id,
                ]));
            }
            if ($request->get("frequency")) {

                $taskEvent->frequency()->save(Frequency::make($request->get("frequency")));
                $taskEvent->tags()->save(TaskTag::create([
                    "name" => TaskTag::getTag('frequency'),
                    "task_event_id" => $taskEvent->id,
                ]));

            }
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }


        $group->members->each(function ($member) use(&$taskEvent){
            $taskEvent->taskOrders()->save(TaskOrder::make([
                "member_id" => $member->id,
                "index" => $taskEvent->getNextIndex()
            ]));
        });

        return response()->json($taskEvent, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Peon\TaskEvent $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group, TaskEvent $taskEvent)
    {
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        return response()->json($taskEvent, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Peon\TaskEvent $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group, TaskEvent $taskEvent)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        return response()->json($taskEvent, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Peon\TaskEvent $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group, TaskEvent $taskEvent)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        $validator = Validator::make($request->all(), [
            "title" => "string|min:1|max:64",
            "description" => "string|max:450",
            "color" => "min:7|max:25",
            "credits" => "integer",
            "order_index" => "integer|min:0",
            "quit_on_expiration" => "boolean",
            "group_id" => "integer",

            "condition" => "array|min:1",
            "expiration" => "array",
            "frequency" => [
                "array",
                "min:1",
                function ($attr, $value, $fail) {
                    foreach($value as $key=>$val){
                        if (!in_array($key, ['day', 'week','month'])) {
                            $fail("Field '$key' is not valid");
                        }
                    }

                },
            ],
            "condition.date" => "date_format:Y-m-d",
            "condition.time" => "date_format:H:i",

            "expiration.expires" => "date_format:H:i",

            "frequency.day" => 'integer',
            "frequency.week" => 'integer',
            "frequency.month" => 'integer'

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);

        }
        foreach(["condition","frequency","expiration"] as $value){
            if($request->has($value)){
                if($taskEvent->$value){
                    $taskEvent->$value->update($request->get($value));
                }
            }
        }

        $update = $taskEvent->update($request->all());
        return response()->json([$update, $taskEvent], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Peon\TaskEvent $taskEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group, TaskEvent $taskEvent)
    {
        $user = auth()->user();
        if(Gate::denies("admin-group",[$group,$user])){
            abort(401);
        }
        try {
            if ($taskEvent->condition) {
                $taskEvent->condition->delete();
            }
            if ($taskEvent->expiration) {
                $taskEvent->expiration->delete();
            }
            if ($taskEvent->frequency) {
                $taskEvent->frequency->delete();
            }
            if ($taskEvent->tasks) {
                $taskEvent->tasks->each(function (Task $task) {
                    $task->delete();
                });
            }
            if ($taskEvent->taskOrders) {
                $taskEvent->taskOrders->each(function (TaskOrder $taskOrder) {
                    $taskOrder->delete();
                });
            }
            if ($taskEvent->tags) {
                $taskEvent->tags->each(function (TaskTag $taskTag) {
                    $taskTag->delete();
                });
            }

            $taskEvent->delete();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
        return response()->json($taskEvent, 200);
    }

    /**
     * @param Request $request
     * @param TaskEvent $taskEvent
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request, Group $group, TaskEvent $taskEvent)
    {
        $validator = Validator::make($request->all(), [
            'on' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            $date = Carbon::parse($request->get("on"));
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }

        $tasks = $this->taskSchedule()->verify()->taskEvent($taskEvent, $date);
        return response()->json($tasks, 200);

    }

    public function finish(Request $request, Group $group, TaskEvent $taskEvent){
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }
        $date = null;
        $validator = Validator::make($request->all(), [
            'on' => '',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            $date = Carbon::parse($request->get("on"));
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
        $member = Member::getMemberByUserInGroup($user,$group);

        try {
            $task = $this->taskSchedule()->finishRunning($taskEvent, $date,$member);
            return response()->json([$task], 200);
        } catch (TaskEventConditionDateException $e) {
            return response()->json($e, 500);
        }
    }


    public function swap(Request $request, Group $group, TaskEvent $taskEvent)
    {

        if(count($taskEvent->taskOrders)==0){
            return response()->json("No TasksOrders are set.", 500);
        }
        $user = auth()->user();
        if(Gate::denies("use-group",[$group,$user])){
            abort(401);
        }

        $validator =  Validator::make($request->all(), [
            "iteration_distance" => "integer|required",
            "member_id" => "integer",
            "swap" => "integer|required",
        ]);

        if (count($validator->errors()) > 0) {
            return response()->json($validator->errors(), 422);
        }

        $attr = $request->all();
        $attr["task_event_id"] = $taskEvent->id;
        $member = Member::getMemberByUserInGroup($user,$group);
        if(!array_key_exists("member_id",$attr)){
            $attr["member_id"] = $member->id;
        }

        $targetIndex = $taskEvent->getIndexIn($request->get("iteration_distance"));
        $targetMember = $taskEvent->taskOrders->where("index",$targetIndex)->first()->member;

        $targetSwapIndex = $taskEvent->getIndexIn($request->get("swap"));
        $targetSwapMember = $taskEvent->taskOrders->where("index",$targetSwapIndex)->first()->member;

        if($targetIndex == $targetSwapIndex){
            $validator->errors()->add("swap","The selected iteration results the same Member. No swapping possible.");
            return response()->json($validator->errors(), 422);
        }

        $bookmark = Bookmark::make([
            "iteration_distance" => $request->get("iteration_distance"),
            "task_event_id" => $request->get("task_event_id"),
            "member_id" => $targetSwapMember->id
        ]);

        $bookmarkSwap = Bookmark::make([
            "iteration_distance" => $request->get("swap"),
            "task_event_id" => $request->get("task_event_id"),
            "member_id" => $targetMember->id
        ]);


        try{
            $taskEvent->bookmarks()->save($bookmark);
            $taskEvent->bookmarks()->save($bookmarkSwap);
        }catch(\Illuminate\Database\QueryException $e){
            $validator->errors()->add("bookmark","A bookmark for this task already exists.");
            return response()->json($validator->errors(), 422);

        }

        return response()->json($bookmark, 201);
    }

}

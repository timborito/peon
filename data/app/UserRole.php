<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;


class UserRole extends Model
{

    protected $fillable = [
        "user_id",
        "role_id",
    ];

    public function role(){
        return $this->belongsTo("Peon\Role");
    }

    public function user(){
        return $this->belongsTo("Peon\User");
    }

}

<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable =[
        "slug",
        "display"
    ];


    public static function getBySlug(String $slug){
        $role = Role::where("slug",$slug)->first();
        return $role;
    }
}

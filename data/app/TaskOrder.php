<?php

namespace Peon;

use Illuminate\Database\Eloquent\Model;

class TaskOrder extends Model
{

    protected $fillable = [
        "member_id",
        "task_event_id",
        "index",
    ];

    public function taskEvent(){
        return $this->belongsTo("Peon\TaskEvent");
    }

    public function member(){
        return $this->belongsTo("Peon\Member");
    }

}

<?php

namespace Peon;

use function foo\func;
use Hamcrest\Util;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Log;
use Carbon\Carbon;
use Validator;
use Peon\Group;
use Illuminate\Validation\Rule;

use Peon\Services\Utilities;

class TaskEvent extends Model
{

    protected $attributes = [
        "title" => "A TaskEvent",
        "description" => "This is a Task description",
        "credits" => 0,
        "order_index" => 0,
        "group_id" => 1,
        "color" => "#FFFFFF",
    ];

    protected $fillable = [
        "title",
        "description",
        "credits",
        "order_index",
        "group_id",
        "color",
    ];

    /** @var Group */
    public function group()
    {
        return $this->belongsTo("Peon\Group");
    }

    /** @var Condition */
    public function condition()
    {
        return $this->hasOne("Peon\Condition");
    }

    public function expiration()
    {
        return $this->hasOne("Peon\Expiration");
    }

    public function taskOrders()
    {
        return $this->hasMany("Peon\TaskOrder")->orderBy("index", "asc");
    }

    public function bookmarks()
    {
        return $this->hasMany("Peon\Bookmark");
    }

    public function frequency()
    {
        return $this->hasOne("Peon\Frequency");
    }

    public function tasks()
    {
        return $this->hasMany("Peon\Task");
    }

    public function tags()
    {
        return $this->hasMany("Peon\TaskTag");
    }

    public function getOrderMember(): Member
    {
        return $this->taskOrders->where("index", $this->order_index)->first()->member;
    }

    public function getConditionDate($reference = null): Carbon
    {
        return $this->condition->getDate($reference);
    }

    public function getExpirationDate($reference = null): Carbon
    {
        return $this->expiration->getDate($reference);
    }

    public function getNextConditionDate($reference = null): Carbon
    {
        return $this->frequency->calc($this->condition->getDate($reference));
    }

    public function getNextExpirationDate($reference = null): Carbon
    {
        return $this->frequency->calc($this->expiration->getDate($reference));
    }

    public function getNextIndex(){
        $lastOrder = $this->taskOrders()->orderBy("index","desc")->get()->last();
        if(!is_null($lastOrder)){
            return $lastOrder->index+1;
        }
        return 0;
    }



    public function getIndexIn(int $iteration){
        $sum = $this->order_index+$iteration;
        $limit = (count($this->taskOrders)-1);
        if($sum > $limit){
            $balance =  $limit - ($this->order_index);
            $iteration -= $balance;
            if($iteration<=count($this->taskOrders)){
                return $iteration-1;
            }
            return ($iteration-1) % count($this->taskOrders) ;
        }
        return $sum;
    }

    /**
     * @param array $attributes
     * @return \Illuminate\Validation\Validator
     */
    public static function validation(array $attributes)
    {
        $validator =  Validator::make($attributes, [
            "title" => "string|min:1|max:64",
            "description" => "string|max:450",
            "color" => "min:7|max:25",
            "credits" => "integer",
            "order_index" => "integer|min:0",
            "quit_on_expiration" => "boolean",
            "group_id" => "integer",

            "condition" => "array|min:1|required_with:frequency",
            "expiration" => "array",
            "frequency" => [
                "array",
                "min:1",
                function ($attr, $value, $fail) {
                    foreach($value as $key=>$val){
                        if (!in_array($key, ['day', 'week','month'])) {
                            $fail("Field '$key' is not valid");
                        }
                    }

                },
            ],
            "condition.date" => "date_format:Y-m-d",
            "condition.time" => "date_format:H:i",

            "expiration.expires" => "required_with:expiration|date_format:H:i",

            "frequency.day" => 'integer',
            "frequency.week" => 'integer',
            "frequency.month" => 'integer'


        ]);

        $validator->after(function ($validator) {
            $cond_time = isset($attributes["time"]) ? $attributes["time"] : null;
            $expi_expires = isset($attributes["expires"]) ? $attributes["expires"] : null;
            $expi_days = isset($attributes["days"])? $attributes["days"] : 0;

            if ($cond_time && $expi_expires) {
                if (is_null($expi_days) || $expi_days === 0)
                    if (Carbon::parse($cond_time)->gte(Carbon::parse($expi_expires))) {
                        $validator->errors()->add('time', 'The expiration time cannot be set before and equel to a condition');
                    }
            }
        });
        return $validator;
    }

    public function delete()
    {
        if($this->tasks){
            $this->tasks->each(function(Task $task){
                $task->delete();
            });
        }
        if($this->tags){
            $this->tags->each(function(TaskTag $tag){
                $tag->delete();
            });
        }
        if($this->taskOrders){
            $this->taskOrders->each(function(TaskOrder $taskOrder){
                $taskOrder->delete();
            });
        }

        if($this->condition){
            $this->condition->delete();
        }
        if($this->expiration){
            $this->expiration->delete();
        }
        if($this->frequency){
            $this->frequency->delete();
        }


        return parent::delete();
    }

}

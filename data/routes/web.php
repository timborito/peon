<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(env("APP_DEBUG")==true) {

    Route::group(['middleware' => ['auth']], function () {

        Route::resource('group', 'GroupController');
        Route::resource('group.member', 'MemberController');
        Route::resource('group.taskEvent', 'TaskEventController');
        Route::resource('group.taskEvent.task', 'TaskController');
        Route::resource('group.taskEvent.bookmark', 'BookmarkController');
        Route::get('/settings', 'SettingsController@index')->name('settings');
    });
//Route::post('/api/register',"API\PassportController@register");
}

Auth::routes();




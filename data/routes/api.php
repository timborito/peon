<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$standardOpts = ['except' => ['create,edit']];

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function () use($standardOpts) {
    Route::resource('group', 'GroupController', $standardOpts);
    Route::post('group/{group}/verify', 'GroupController@verify');
    Route::post('group/{group}/outlook', 'GroupController@outlook');
    Route::resource('group.member', 'MemberController', $standardOpts);
    Route::post('group/{group}/member/{member}/outlook', 'MemberController@outlook');
    Route::resource('group.taskEvent', 'TaskEventController', $standardOpts);
    Route::post('group/{group}/taskEvent/{taskEvent}/finish', 'TaskEventController@finish');
    Route::post('group/{group}/taskEvent/{taskEvent}/swap', 'TaskEventController@swap');
    Route::resource('group.taskEvent.task', 'TaskController', $standardOpts);
    Route::resource('group.taskEvent.bookmark', 'BookmarkController', $standardOpts);

});





